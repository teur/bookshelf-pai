const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
require('dotenv').config();

const app = express();
const port = process.env.PORT;

// TODO: write a middleware thats saves user id
app.use('/', (req, res, next) => {
  try {

  } catch (e) {

  }
});

app.use(express.json());
app.use(cors());
app.use('/books', require('./routes/books'));
app.use('/user', require('./routes/user'));

const start = async () => {
  try {
    await mongoose.connect(
      process.env.DB_URI,
      {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false
      }
    );
    app.listen(port, () => console.log(`server started at ${port}`));
  } catch (e) {
    console.log(e);
  }
};

start();
