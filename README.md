# bookshelf api

## TODO list:
 - [ ] jwt token validation
 - [ ] save token to cookies
 - [ ] middleware thats saves user id to request body
 - [ ] corner
 - [ ] links form books to user
 - [ ] links form user to books
 - [ ] commentaries
 - [ ] wrap api into docker
 - [ ] simple front-end
 - [ ] write some tests
 - [ ] setup ci\cd

## Features:
 - [x] authorization
 - [x] books endpoints
