const {Router} = require('express');
const {body, validationResult} = require('express-validator');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const User = require('../models/user');

const router = Router();

router.post(
  '/login',
  body('username').isLength({min: 3, max: 12}),
  body('password').isLength({min: '6', max: 12}),
  async (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(400).json(errors.array());
      }

      const {username, password} = req.body;
      const candidate = await User.findOne({username});

      if (!candidate) {
        return res.status(404).json({msg: 'could not find user with the same username'});
      }

      const isPasswordValid = await bcrypt.compare(password, candidate.password);

      if (!isPasswordValid) {
        return res.status(400).json({msg: 'username or password is incorrect'});
      }

      const token = jwt.sign({username}, process.env.JWT_SECRET, {
        expiresIn: '8h',
      });

      res.status(200).json({userId: candidate._id, token});
    } catch (e) {
      res.status(400).json({errors: e});
    }
  });

router.post(
  '/register',
  body('username').isLength({min: 3, max: 12}),
  body('email').isEmail(),
  body('password').isLength({min: '6', max: 12}),
  async (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(400).json(errors.array());
      }


      const {username, email, password} = req.body;
      const candidate = await User.findOne({username});

      if (candidate) {
        return res.status(400).json({msg: 'user with the same username is already exists'});
      }

      const hashedPassword = await bcrypt.hash(password, Number(process.env.BCRYPT_HASH));

      const user = new User({username, email, password: hashedPassword});
      await user.save();

      res.status(201).json({msg: 'user created'});
    } catch (e) {
      res.status(400).json({errors: e});
    }
  });

module.exports = router;
