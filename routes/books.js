const {Router} = require('express');
const {body, validationResult} = require('express-validator');
const Book = require('../models/books');

const router = Router();

router.post(
  '/add',
  body('title').isLength({min: 2, max: 64}),
  body('author').isLength({min: 2, max: 64}),
  body('description').isLength({min: 32, max: 1024}),
  body('capture').isLength({min: 16, max: 1024}),
  async (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(400).json({errors});
      }
      const {
        title,
        author,
        description,
        capture,
      } = req.body;
      const book = new Book({title, author, description, capture});
      await book.save();
      res.status(201).json({msg: 'book created'});
    } catch (e) {
      res.status(400).json({err});
    }
  });

router.post(
  '/remove/:id',
  async (req, res) => {
    try {
      const {id} = req.params;
      await Book.findOneAndDelete(id);
      res.status(200).json({msg: 'book deleted'});
    } catch (err) {
      res.status(400).json({err});
    }
  });

router.post(
  '/edit',
  body('title').isLength({min: 2, max: 64}),
  body('author').isLength({min: 2, max: 64}),
  body('description').isLength({min: 32, max: 1024}),
  body('capture').isLength({min: 16, max: 1024}),
  body('id').notEmpty(),
  async (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(400).json({errors});
      }
      const {id} = req.body;
      delete req.body.id;
      await Book.findOneAndUpdate(id, req.body);
      res.status(200).json({msg: 'book updated'});
    } catch (err) {
      res.status(400).json({err});
    }
  });

module.exports = router;
