const {Schema, model} = require('mongoose');

const Book = new Schema({
  title: {type: String, required: true},
  author: {type: String, required: true},
  description: {type: String, required: true},
  capture: {type: String, rquired: true},
  user: {type: Schema.Types.ObjectId, ref: 'User'},
});

module.exports = model('Book', Book);
